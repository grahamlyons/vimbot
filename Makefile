deploy: options.json
	git push deploy -f

options.json:
	cp secret.json options.json
	git add options.json
	git commit -m 'Adding options.json for deployment'

push: remove_options.json
	git push origin master

remove_options.json:
	git filter-branch -f --index-filter 'git rm --cached --ignore-unmatch options.json' HEAD
