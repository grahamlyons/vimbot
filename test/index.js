var assert = require('assert'),
    tst = require('tst'),
    fs = require('fs'),
    getDocument = require('../tasks/model-data').getDocument;

tst('Test tweet no urls or hashtags', function() {
    var tweet = {
        text: 'Hello World'
    };
    var doc =  getDocument(tweet);
    assert.equal(doc, tweet.text);
});

tst('Test tweet with one url', function() {
    var tweet = {
        text: 'RT @xah_lee: the effect of keyboard on your typing keybinding habit http://t.co/LkzhxDYiss',
        entities: {
            urls: [ {
                url: 'http://t.co/LkzhxDYiss',
                expanded_url: 'http://xahlee.blogspot.com/2013/06/the-effect-of-keyboard-hardware-on-your.html',
                display_url: 'xahlee.blogspot.com/2013/06/the-ef…',
                indices: [ 68, 90 ]
            } ]
        }
    };
    var doc = getDocument(tweet);
    var expected = 'RT @xah_lee: the effect of keyboard on your typing keybinding habit xahlee.blogspot.com/2013/06/the-effect-of-keyboard-hardware-on-your.html'
    assert.equal(doc, expected);
});

tst('Test tweet with two urls', function() {
    var tweet = {
        text: 'RT @xah_lee: the effect of keyboard on http://t.co/LkzTxDKiss your typing keybinding habit http://t.co/LkzhxDYiss',
        entities: {
            urls: [ {
                url: 'http://t.co/LkzTxDKiss',
                expanded_url: 'http://www.example.com/an-interesting-page',
                display_url: 'www.example.com/an-interesting-page',
                indices: [ 39, 61 ]
            }, {
                url: 'http://t.co/LkzhxDYiss',
                expanded_url: 'http://xahlee.blogspot.com/2013/06/the-effect-of-keyboard-hardware-on-your.html',
                display_url: 'xahlee.blogspot.com/2013/06/the-ef…',
                indices: [ 90, 112 ]
            } ]
        }
    };
    var doc = getDocument(tweet);
    var expected = 'RT @xah_lee: the effect of keyboard on your typing keybinding habit www.example.com/an-interesting-page xahlee.blogspot.com/2013/06/the-effect-of-keyboard-hardware-on-your.html'
    assert.equal(doc, expected);
});
