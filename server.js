/*require('strong-agent').profile(
    'b7455e2e83b65a620bcd189cbd498168',
    'vimbot'
);*/
var http = require('http'),
    fs = require('fs'),
    Router = require('rtr'),
    swig = require('swig'),
    monk = require('monk'),
    scrape = require('./lib/scrape'),
    options = require('./options');

var TRACK = '#vim';
var mongoHost = process.env.OPENSHIFT_MONGODB_DB_HOST || process.env.IP || 'localhost';
var mongoPort = process.env.OPENSHIFT_MONGODB_DB_PORT || 27017;
var user = process.env.OPENSHIFT_MONGODB_DB_USERNAME || null;
var pass = process.env.OPENSHIFT_MONGODB_DB_PASSWORD || null;
var db = monk(mongoHost + ':' + mongoPort + '/vimbot', {username: user, password: pass}).get('tweets');
var DS = '/';
var baseDir = getBaseDir(module.filename);
var templatePath = getTemplatePath(baseDir, 'templates');
var MIME_TYPES = {
    'css': 'text/css',
    'js': 'text/javascript',
    'json': 'application/json'
};
var ipaddr  = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || "0.0.0.0";
var port    = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8888;

function getBaseDir(moduleFilename) {
    var parts = moduleFilename.split(DS);
    parts.pop();
    return parts.join(DS);
}

function getTemplatePath(base, templateDirectory) {
    var parts = base.split(DS);
    parts.push(templateDirectory);
    return parts.join(DS);
}

var router = new Router(handle404);

function handle404(request, response) {
    response.writeHead(404);
    response.end('Not Found');
}

function handle200(response, content) {
    response.writeHead(200);
    response.end(content);
}

function handle500(request, response, message) {
    response.writeHead(500);
    message = message ? message:'Internal Server Error';
    response.end(message);
}

router.get('/', index);
router.get('/all', all);
router.get(new RegExp('^/static/(.+)$'), serveStatic);

function index(request, response) {
    response.setHeader('Content-Type', 'text/html;charset=utf-8');
    try{
        db.find(
            {classification: "1"},
            {limit: 10, sort: {'created_at': -1}},
            function(err, results) {
                if(err) {
                    throw err;
                }
                handle200(response, swig.renderFile(
                    templatePath + '/index.html',
                    {results: results, track: TRACK})
                );
        });
    } catch(e) {
        handle500(request, response, e.message);
    }
}

function all(request, response) {
    response.setHeader('Content-Type', 'application/json');
    try{
        db.find(function(err, results) {
            if(err) {
                throw err;
            }
            response.writeHead(200);
            results.forEach(function(tweet) {
                response.write(JSON.stringify(tweet) + '\n');
            });
            response.end();
        });
    } catch(e) {
        handle500(request, response, e.message);
    }
}

function serveStatic(request, response, filepath) {
    var staticFile = baseDir + DS + 'static' + DS + filepath;
    if(fs.existsSync(staticFile)) {
        var contentType = MIME_TYPES[staticFile.split('.').pop()];
        fs.readFile(staticFile, function(err, data) {
            if (err) {
                handle500(request, response);
            } else {
                response.setHeader('Content-Type', contentType);
                handle200(response, data.toString());
            }
        });
    } else {
        handle404(request, response);
    }
}

var server = http.createServer(router.dispatch);

server.listen(port, ipaddr);
console.log('Running on %s:%s', ipaddr, port);

scrape(options.twitterAuth, TRACK, db);
