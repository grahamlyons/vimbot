var monk = require('monk');

var connection = monk(process.env.OPENSHIFT_MONGODB_DB_URL + 'vimbot')
var db = connection.get('tweets');

var results = db.find({'created_at': {$type: 2}}, {limit: 500});

results.then(function(docs) {
    docs.forEach(function(doc) {
         if(typeof(doc.created_at) == 'string') {
             console.log("TYPE", typeof(doc.created_at));
             var newDate = new Date(doc.created_at);
             db.update(doc._id, {$set: {'created_at': newDate}});
         }
    });
}, function(err){
    console.error("ERROR", err);
}).then(function(){
    console.log("Closing...")
    connection.close();
});