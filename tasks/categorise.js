var readline = require('readline'),
    LineBuffer = require('line_buffer'),
    fs = require('fs'),
    http = require('http');

var url = process.argv.length == 3 ? process.argv[2] : 'http://vimbot-grahamlyons.rhcloud.com/all';

var comms = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var tb = new LineBuffer();
var lines = [];
tb.on('line', function(line) {
    lines.push(line);
});

var yesFile = fs.createWriteStream('./YES.tmp');
var noFile = fs.createWriteStream('./NO.tmp');

http.get(url, function (response) {
    response.on('readable', function() {
        console.log('Reading data from %s', url);
        var data;
        while((data = response.read(100)) != null) {
            tb.write(data);
        }
    });
    response.on('end', function() {
        ask(lines);
    });
});

function ask(lines) {
    var line = lines.pop();
    var tweet, message;
    try {
        tweet = JSON.parse(line);
    } catch(e) { console.error(e); }
    if(tweet) {
        message = tweet.text;
        if(tweet.entities && tweet.entities.urls) {
            var urls = tweet.entities.urls;
            message + '\n\t' + urls.join(' ');
        }
        comms.question(message + '\n', function(answer) {
            line = line + '\n';
            if(answer == 'y') {
                yesFile.write(line);
            } else {
                noFile.write(line);
            }
            if(lines.length) {
                ask(lines);
            }
        });
    } else {
        if(lines.length) {
            ask(lines);
        }
    }
}
