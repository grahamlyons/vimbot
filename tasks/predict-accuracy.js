var Data = require('../lib/ingest-data'),
    Classifier = require('../lib/classifier');

var data = new Data('../data/YES', '../data/NO');
var N = 10;

data.on('ready', function(data) {
    console.log('Ready', data.length);

    var trainingSet;
    var testingSet;
    var correct = 0;
    var performance = {
        tp: 0,
        tn: 0,
        fp: 0,
        fn: 0
    };
    var setSize = parseInt(data.length/N);
    var classifier;
    data = data.slice(data.length % setSize);

    var start = 0;
    for(var index = setSize; index <= data.length; index += setSize) {
        console.log('Running set ', index);
        trainingSet = data.slice(0, start).concat(data.slice(index));
        testingSet = data.slice(start, index);
        classifier = new Classifier(trainingSet);
        correct = performRun(classifier, testingSet, correct, performance);
        start += setSize;
    }

    console.log(correct, data.length);
    console.log('Precision', 100 * (performance.tp / (performance.tp + performance.fp)));
    console.log('Recall', 100 * (performance.tp / (performance.tp + performance.fn)));
    console.log('Specificity', 100 * (performance.tn / (performance.tn + performance.fp)));
    console.log('Accurarcy', 100 * ((performance.tn + performance.tp) / (performance.tn + performance.fp + performance.tp + performance.fn)));

    console.log(performance.tp + performance.tn + performance.fp + performance.fn);
});

function performRun(classifier, testingSet, correct, performance) {

    testingSet.forEach(function(doc) {
        var classification = classifier.classify(doc.text);
        if(classification == doc.classification) {
            correct++;
            if(parseInt(classification) == 1) {
                performance.tp++;
            } else {
                performance.tn++;
            }
        } else {
            if(parseInt(classification) == 1) {
                performance.fp++;
            } else {
                performance.fn++;
            }
        }
    });

    return correct;
}
