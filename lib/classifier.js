var natural = require('natural'),
    getDocument = require('./model-data').getDocument;

function Classifier(trainingSet) {

    this.classifier = new natural.BayesClassifier();

    var self = this;
    trainingSet.forEach(function(doc) {
        self.classifier.addDocument(doc.text, doc.classification);
    });

    this.classifier.train();

}

Classifier.prototype.classify = function(tweet) {
    var doc = getDocument(tweet);
    return this.classifier.classify(doc);
}

module.exports = Classifier;
