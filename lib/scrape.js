var twitter = require('twitter'),
    BatchClassify = require('./classify');

module.exports = scrape;

function scrape(twitterAuth, track, db) {

    var twit = new twitter(twitterAuth);
    var batch = new BatchClassify(db);

    twit.stream('statuses/filter', {track: track}, function(stream) {

        stream.on('data', function(data) {
            if (data.user.lang === 'en') {
                console.log('Received data', data.text, data.created_at);
                data.created_at = new Date(data.created_at);
                db.insert(data, function (err, doc) {
                    if (err) {
                        throw err;
                    } else {
                        console.log('Inserted data', doc._id);
                    }
                });
                batch.run();
            }
        });

        stream.on('error', function(error) {
            console.error(error);
        });

    });

} 