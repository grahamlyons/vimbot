var Classifier = require('./classifier'),
    Data = require('./ingest-data'),
    Q = require('q');

function BatchClassify(db) {

    this.db = db;
    this.deferredClassifier = Q.defer();
    this.classifier = this.deferredClassifier.promise;

    var data = new Data('./data/YES', './data/NO');
    var self = this;
    var onReady = function(data) {
        self.deferredClassifier.resolve(new Classifier(data));
    };

    data.on('ready', onReady);

}

BatchClassify.prototype.run = function() {
    this.db.find(
        {classification: {$exists: false}},
        {limit: 10, sort: 'created_at'},
        this._dbFound.bind(this)
    );
}

BatchClassify.prototype._dbFound = function(err, docs) {
    if(err) {
        throw err;
    }
    var self = this;
    var classifyAndUpdate = function(tweet) {
        return function(classifier) {
            self.db.update(
                tweet._id,
                {$set:
                    {classification: classifier.classify(tweet)}
                }
            );
        };
    };

    docs.forEach(function(tweet) {
        self.classifier.then(classifyAndUpdate(tweet)).done();
    });
}

module.exports = BatchClassify;
