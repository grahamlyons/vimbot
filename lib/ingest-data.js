var fs = require('fs'),
    util = require('util'),
    EventEmitter = require('events').EventEmitter,
    LineBuffer = require('line_buffer'),
    getDocument = require('./model-data').getDocument; 

function Data(yesFileName, noFileName) {
    this.readyEvent = 'ready';
    this.data = [];
    this.done = 0;
    var files = 2;
    var self = this;

    var yesLb = new LineBuffer();
    var noLb = new LineBuffer();
    var yesFile = fs.createReadStream(yesFileName);
    var noFile = fs.createReadStream(noFileName);

    yesFile.on('end', endHandler);
    noFile.on('end', endHandler);

    yesLb.on('line', getDocHandler(1));
    noLb.on('line', getDocHandler(0));

    yesFile.on('readable', getDataHandler(yesFile, yesLb));
    noFile.on('readable', getDataHandler(noFile, noLb));

    function endHandler() {
        self.done++;
        if(self.done == files) {
            self.emit(self.readyEvent, self.data);
        }
    }

    function getDocHandler(classification) {
        return function(line) {
            var text = getDocument(JSON.parse(line));
            self.data.push({classification: classification, text: text});
        };
    }

    function getDataHandler(fileRS, lb) {
        return function() {
            var data;
            while((data = fileRS.read(100)) != null) {
                lb.write(data);
            }
        };
    }

}

util.inherits(Data, EventEmitter);

module.exports = Data;
