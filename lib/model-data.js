var urlParse = require('url').parse,
    natural = require('natural'),
    stopwords = require('../lib/stopwords').concat(natural.stopwords);

function getDocument(tweet) {
    var text = tweet.text;
    var e = tweet.entities;
    if(e) {
        var urls = e.urls;
        if(urls) {
            var urlsToAppend = urls.map(function(url) {
                var parts = urlParse(url.expanded_url);
                return [parts.hostname, parts.pathname].join('');
            });

            var modifier = 0;
            urls.forEach(function(url) {
                var tm = removeUrl(text, url, modifier);
                text = tm.text;
                modifier = tm.modifier;
            });
            var urlSuffix = urlsToAppend.join(' ');
            modifier += urlSuffix.length;
            text += urlSuffix;
        }

    }
    text = tokeniseAndRemoveStops(text);
    text = getBiGrams(text);
    return text;
}

function removeUrl(text, urlData, indexModifier) {
    var firstIndex, secondIndex, before, after;
    firstIndex = urlData.indices[0] + indexModifier;
    secondIndex = urlData.indices[1] + indexModifier;
    before = text.slice(0, firstIndex);
    after = text.slice(secondIndex + 1);
    indexModifier -= secondIndex - firstIndex;
    return {text: before + after, modifier: indexModifier};
}

function tokeniseAndRemoveStops(text) {
    if(!(text instanceof Array)) {
        text = text.split(' ');
    }
    return text.filter(
        function(word) {
            return stopwords.indexOf(word) == -1;
        }
    );
}

function getBiGrams(text) {
    var bigrams = natural.NGrams.bigrams(text).map(
        function(bg) { return bg.join(' '); }
    );
    return bigrams;
}

module.exports.getDocument = getDocument;
